# License

tldr: Use the code and assets developed for this game however you want. It
would be nice if you link back to my pages and projects to share the love.

External assets you'll have to provide attribution

NVIDIA flex packages under Content/TestPackages/Flex are their code and
you'll have to refer to their docs


## Code
See LICENSE/MIT.txt

## Assets
Assets developed by me for this jam: see LICENSE/CC-BY-4.0.txt

Third party assets such as:
The song "Salt Mines" by Meizong:
- see LICENSE/CC-BY-3.0.txt
- Their music can be found here https://soundcloud.com/dj-meizong/salt-mines
The font Grishenko Opiyat NBP:
- This font was created by total FontGeek DTF, Ltd.
- LICENSE is indicated as cc-by-sa on:
  https://www.fontspace.com/grishenko-opiyat-nbp-font-f17903
The font Motorik NBP:
- This font was created by total FontGeek DTF, Ltd.
- LICENSE is indicated as cc-by-sa on:
  https://www.fontspace.com/motorik-nbp-font-f15522
The bass guitar loop at: ThirdPartyAssets/looperman-l-4597371-0255179-overdriven-hot-metalcore-bass-gtr-84.wav
- This was provided copyright free from looperman at: https://www.looperman.com/loops/detail/255179/overdriven-hot-metalcore-bass-gtr-84-free-84bpm-heavy-metal-bass-guitar-loop
- I've clipped a piece of this audio to create the asset small_fail_sound
The synth track at: ThirdPartyAssets/looperman-l-1314288-0255635-perfect-nightclub20xx.wav
- This was provided copyright free from looperman at: https://www.looperman.com/loops/detail/255635/perfect-nightclub20xx-free-164bpm-trap-synth-loop
- I've clipped a piece to create the asset triumph_tune_v1
The sound effect at: 442327__bolkmar__fx-retro-videogame-click-menu-option.wav
- If free to use under CC0-1.0
- It can be downloaded at: https://freesound.org/people/bolkmar/sounds/442327
- This effect was used to create the asset click_click_sfx



## NVIDIA FLEX
NVIDIA FLEX assets are likely available under the NVIDIA Source Code License
referenced at:
https://github.com/NVIDIAGameWorks/FleX/blob/master/LICENSE.txt

or they may fall under the Unreal Engine EULA because they are a distributed
with a custom build of the Engine

Information on both of these in the ./LICENSE folder
